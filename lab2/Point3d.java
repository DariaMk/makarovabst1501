package lab2;

public class Point3d {
	 /** X coordinate of the point **/
    private double xCoord;
    
    /** Y coordinate of the point **/
    private double yCoord;
    
    /** Z coordinate of the point **/
    private double zCoord;

    /** Constructor to initialize point to (x, y) value. **/
    public Point3d(double x, double y, double z) {
        xCoord = x;
        yCoord = y;
        zCoord = z;
    }

    /** No-argument constructor:  defaults to a point at the origin. **/
    public Point3d() {
        // Call two-argument constructor and specify the origin.
        this(0.0, 0.0, 0.0);
    }

    /** Return the X coordinate of the point. **/
    public double getX() {
        return xCoord;
    }

    /** Return the Y coordinate of the point. **/
    public double getY() {
        return yCoord;
    }

    /** Return the Z coordinate of the point. **/
    public double getZ() {
        return zCoord;
    }
    /** Set the X coordinate of the point. **/
    public void setX(double val) {
        xCoord = val;
    }

    /** Set the Y coordinate of the point. **/
    public void setY(double val) {
        yCoord = val;
    }
    
    /** Set the Z coordinate of the point. **/
    public void setZ(double val) {
        zCoord = val;
    }
    
    public boolean equals (Point3d �) {
		if((this.xCoord == �.getX()) && (this.yCoord == �.getY()) &&
				(this.zCoord == �.getZ())) {
			return true;}
		else { return false;}
				
	}
    public double distanceTo (Point3d �) {
		return Math.sqrt(Math.pow((this.xCoord - �.getX()), 2)+
				Math.pow(this.yCoord - �.getY(), 2) +
				Math.pow(this.zCoord - �.getZ(), 2));
	}
}





